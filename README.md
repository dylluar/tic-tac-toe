# Tic-tac-toe (Crazy)

A small hobbye project made with VueJs :heart:

## See working
<https://domeniqque.github.io/tic-tac-toe>

## Multiplayer mode

![image](https://raw.githubusercontent.com/Domeniqque/tic-tac-toe/master/static/img/tic-tac-toe-multiplayer.png)

## Against a robot
![image](https://raw.githubusercontent.com/Domeniqque/tic-tac-toe/master/static/img/tic-tac-toe-robot.png)

## Google Audits
![image](https://raw.githubusercontent.com/Domeniqque/tic-tac-toe/master/static/img/tic-tac-toe-audits.png)

## Stack
* VueJs
* Async and await (promises)
* PWA
* WebWorkers
* CSS3 (only)

## To run

``` bash
After clone this project

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```
